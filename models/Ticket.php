<?php
    class Ticket extends Connect{

        public function insert_ticket($user_id, $cat_id, $tick_name, $tick_descrip){

            $connect = parent::connection();
            parent::set_name();

            $sql = "INSERT INTO tb_ticket (
                tick_id, 
                user_id, 
                cat_id, 
                tick_name, 
                tick_descrip, 
                tick_status, 
                created_at, status) VALUES (NULL,?,?,?,?,'Opened',now(),'1')";

            $sql = $connect->prepare($sql);

            $sql->bindValue(1, $user_id);
            $sql->bindValue(2, $cat_id);
            $sql->bindValue(3, $tick_name);
            $sql->bindValue(4, $tick_descrip);

            $sql->execute();

            return $result=$sql->fetchAll();
        }

        public function get_data($user_id){

            $connect = parent::connection();
            parent::set_name();

            // SQL JOIN TABLE USER, TICKET, CATEGORY
            $sql = "SELECT 
            
            tb_ticket.tick_id, 
            tb_ticket.user_id, 
            tb_ticket.cat_id, 
            tb_ticket.tick_name, 
            tb_ticket.tick_descrip, 
            tb_ticket.tick_status, 
            tb_ticket.created_at, 
            tb_users.user_fullname, 
            
            tb_category.cat_name FROM tb_ticket INNER JOIN tb_category ON 
            tb_ticket.cat_id = tb_category.cat_id INNER JOIN tb_users ON 
            tb_ticket.user_id = tb_users.user_id WHERE tb_ticket.status = 1 AND tb_users.user_id=?";

            $sql = $connect->prepare($sql);
            $sql->bindValue(1, $user_id);
            $sql->execute();
            return $result = $sql->fetchAll();
        }

        public function get_data_petugas(){
            $connect = parent::connection();
            parent::set_name();

            // SQL JOIN TABLE USER, TICKET, CATEGORY
            $sql = "SELECT
            tb_ticket.tick_id, 
            tb_ticket.user_id, 
            tb_ticket.cat_id, 
            tb_ticket.tick_name, 
            tb_ticket.tick_descrip, 
            tb_ticket.tick_status, 
            tb_ticket.created_at, 
            tb_users.user_fullname, 

            tb_category.cat_name FROM tb_ticket INNER JOIN tb_category ON 
            tb_ticket.cat_id = tb_category.cat_id INNER JOIN tb_users ON 
            tb_ticket.user_id = tb_users.user_id WHERE tb_ticket.status = 1";

            $sql = $connect->prepare($sql);
            $sql->execute();
            return $result = $sql->fetchAll();
        }
    }